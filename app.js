/* 
 * Copyright (C) 2016.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

// app.js

// Generate a new instance of express server.
var express = require('express'),
        config = require(__dirname + '/config.js'),
        http = require('http');
fs = require('fs');
var voteModel = require('./voteModel.js');
var app = express();
var dbName = './database.json';


app.use(express.bodyParser());
app.use(express.urlencoded());
app.use(express.json());

// Starts the server itself
var server = http.createServer(app).listen(config.server.port, config.server.host, function () {
    console.log("Nodejs Server listening to %s:%d within %s environment",
            config.server.host,
            config.server.port,
            app.get('env'));
});
//=====================  end =================================================



//Checking that server is working or not.
app.get('/votes', function (req, res) {
    fs.readFile(dbName,function (err,db) {
       var listVotes = JSON.parse(db.toString());
        console.log(listVotes);
        res.json(listVotes);
    });

});

app.post('/votes', function (req, res) {
    var vote = new voteModel();

    vote.value = req.param('vote');
    vote.userName = req.param('userName');
    vote.date = req.param('date');

    fs.readFile(dbName,function(err,db){
        var database = JSON.parse(db.toString());
        database.push(vote.toString());

        console.log(vote);

        console.log(database);

        fs.writeFile(dbName, JSON.stringify(database), function () {
            res.json(vote);
        });
    })
});